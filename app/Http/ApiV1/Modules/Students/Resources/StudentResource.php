<?php

namespace App\Http\ApiV1\Modules\Students\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
/** @mixin \App\Domain\Users\Models\User */
class StudentResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'birthday' => $this->birthday,
            'dormitory' => $this->dormitory,
            'group_id' => $this->group_id,
        ];
    }
}
