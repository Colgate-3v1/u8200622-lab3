<?php
namespace App\Http\ApiV1\Modules\Students\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class ReplaceStudentRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'full_name' => 'required|string|max:50',
            'birthday' => 'required|date_format:Y-m-d',
            'dormitory' => 'required|boolean',
            'group_id' => 'required|integer|exists:groups,id'
        ];
    }
}