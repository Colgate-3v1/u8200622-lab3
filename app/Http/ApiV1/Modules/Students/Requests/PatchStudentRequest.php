<?php
namespace App\Http\ApiV1\Modules\Students\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchStudentRequest extends BaseFormRequest
{
    public function rules(): array
    {
        
        return [
            'full_name' => 'string|max:50',
            'birthday' => 'date_format:Y-m-d',
            'dormitory' => 'boolean',
            'group_id' => 'integer|exists:groups,id'
        ];
    }
}