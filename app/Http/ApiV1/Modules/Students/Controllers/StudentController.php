<?php

namespace App\Http\ApiV1\Modules\Students\Controllers;

use Illuminate\Http\Request;

use App\Http\ApiV1\Modules\Students\Requests\CreateStudentRequest;
use App\Http\ApiV1\Modules\Students\Requests\PatchStudentRequest;
use App\Http\ApiV1\Modules\Students\Requests\ReplaceStudentRequest;

use App\Http\ApiV1\Modules\Students\Resources\StudentResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;


use App\Domain\Students\Actions\GetAllStudentAction;
use App\Domain\Students\Actions\GetStudentAction;
use App\Domain\Students\Actions\CreateStudentAction;
use App\Domain\Students\Actions\DeleteStudentAction;
use App\Domain\Students\Actions\PatchStudentAction;
use App\Domain\Students\Actions\ReplaceStudentAction;


class StudentController
{
    public function getlist(GetAllStudentAction $action)
    {
        $students = $action->execute();
        return response()->json(["data" => $students]);
    }

    public function get(int $studentId, GetStudentAction $action)
    {
        return new StudentResource($action->execute($studentId));
    }

    public function post(CreateStudentAction $action, CreateStudentRequest $request)
    {
        return new StudentResource($action->execute($request->validated()));
    }

    public function delete(int $studentId, DeleteStudentAction $action)
    {
        $action->execute($studentId);
        return new EmptyResource();
    }

    public function patch(int $studentId, PatchStudentAction $action, PatchStudentRequest $request)
    {
        return new StudentResource($action->execute($studentId, $request->validated()));
    }

    public function put(int $studentId, ReplaceStudentAction $action, ReplaceStudentRequest $request)
    {
        return new StudentResource($action->execute($studentId, $request->validated()));
    }
}
