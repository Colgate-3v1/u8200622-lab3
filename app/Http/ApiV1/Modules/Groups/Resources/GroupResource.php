<?php

namespace App\Http\ApiV1\Modules\Groups\Resources;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class GroupResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'direction' => $this->direction,
            'course_number' => $this->course_number,
            'tutor_name' => $this->tutor_name,
        ];
    }
}
