<?php
namespace App\Http\ApiV1\Modules\Groups\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchGroupRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'direction' => 'string|max:50',
            'course_number' => 'integer|between:1,5',
            'tutor_name' => 'string|max:50'
        ];
    }
}
