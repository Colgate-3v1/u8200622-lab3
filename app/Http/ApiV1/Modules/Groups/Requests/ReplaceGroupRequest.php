<?php
namespace App\Http\ApiV1\Modules\Groups\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ReplaceGroupRequest extends BaseFormRequest
{
    public function rules(): array
    {
        {
            return [
                'direction' => 'required|string|max:50',
                'course_number' => 'required|integer|between:1,5',
                'tutor_name' => 'required|string|max:50'
            ];
        }
        
    }
}
