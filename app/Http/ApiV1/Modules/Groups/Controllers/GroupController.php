<?php

namespace App\Http\ApiV1\Modules\Groups\Controllers;

use Illuminate\Http\Request;

use App\Http\ApiV1\Modules\Groups\Requests\CreateGroupRequest;
use App\Http\ApiV1\Modules\Groups\Requests\PatchGroupRequest;
use App\Http\ApiV1\Modules\Groups\Requests\ReplaceGroupRequest;


use App\Domain\Groups\Actions\GetAllGroupAction;
use App\Domain\Groups\Actions\GetGroupAction;
use App\Domain\Groups\Actions\CreateGroupAction;
use App\Domain\Groups\Actions\DeleteGroupAction;
use App\Domain\Groups\Actions\PatchGroupAction;
use App\Domain\Groups\Actions\ReplaceGroupAction;

use App\Http\ApiV1\Modules\Groups\Resources\GroupResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;


class GroupController
{
    public function getlist(GetAllGroupAction $action)
    {
        $groups = $action->execute();
        return response()->json(["data" => $groups]);
    }

    public function get(int $groupId, GetGroupAction $action)
    {
        return new GroupResource($action->execute($groupId));
    }

    public function post(CreateGroupAction $action, CreateGroupRequest $request)
    {
        return new GroupResource($action->execute($request->validated()));
    }

    public function delete(int $groupId, DeleteGroupAction $action)
    {   
        $action->execute($groupId);
        return new EmptyResource();
    }

    public function patch(int $groupId, PatchGroupAction $action, PatchGroupRequest $request)
    {
        return new GroupResource($action->execute($groupId, $request->validated()));
    }

    public function put(int $groupId, ReplaceGroupAction $action, ReplaceGroupRequest $request)
    {
        return new GroupResource($action->execute($groupId, $request->validated()));
    }
}
