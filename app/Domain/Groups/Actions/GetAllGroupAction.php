<?php

namespace App\Domain\Groups\Actions;

use App\Domain\Groups\Models\Group;

class GetAllGroupAction
{
    public function execute(): array
    {
        return Group::all()->toArray();
    }
}
