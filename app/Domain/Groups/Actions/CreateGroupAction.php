<?php

namespace App\Domain\Groups\Actions;

use App\Domain\Groups\Models\Group;

class CreateGroupAction
{
    public function execute(array $fields): Group
    {
        return Group::create($fields);
    }
}
