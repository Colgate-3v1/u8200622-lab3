<?php

namespace App\Domain\Groups\Actions;

use App\Domain\Groups\Models\Group;

class PatchGroupAction
{
    public function execute(int $groupId, array $fields): Group
    {
        $group = Group::findOrFail($groupId);
        $group->update($fields);
        return $group;
    }
}
