<?php

namespace App\Domain\Groups\Actions;

use App\Domain\Groups\Models\Group;

class DeleteGroupAction
{
    public function execute(int $groupId)
    {
        $group = Group::findOrFail($groupId);
        $group->delete();
    }
}
