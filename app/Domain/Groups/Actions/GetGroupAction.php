<?php

namespace App\Domain\Groups\Actions;

use App\Domain\Groups\Models\Group;

class GetGroupAction
{
    public function execute(int $groupId): Group
    {
        return Group::findOrFail($groupId);
    }
}
