<?php

namespace App\Domain\Groups\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'groups';
    
    protected $fillable = [
        'direction', 'course_number', 'tutor_name',
    ];

    public function students()
    {
        return $this->hasMany(Student::class);
    }
}
