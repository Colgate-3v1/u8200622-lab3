<?php

namespace App\Domain\Students\Actions;

use App\Domain\Students\Models\Student;

class DeleteStudentAction
{
    public function execute(int $studentId)
    {
        $student = Student::findOrFail($studentId);
        $student->delete();
    }
}
