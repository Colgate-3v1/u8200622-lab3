<?php

namespace App\Domain\Students\Actions;

use App\Domain\Students\Models\Student;

class CreateStudentAction
{
    public function execute(array $fields): Student
    {
        return Student::create($fields);
    }
}
