<?php

namespace App\Domain\Students\Actions;

use App\Domain\Students\Models\Student;

class GetStudentAction
{
    public function execute(int $studentId): Student
    {
        return Student::findOrFail($studentId);
    }
}
