<?php

namespace App\Domain\Students\Actions;

use App\Domain\Students\Models\Student;

class GetAllStudentAction
{
    public function execute(): array
    {
        return Student::all()->toArray();
    }
}
