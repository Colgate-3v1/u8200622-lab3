<?php

namespace App\Domain\Students\Actions;

use App\Domain\Students\Models\Student;

class PatchStudentAction
{
    public function execute(int $studentId, array $fields): Student
    {
        $student = Student::findOrFail($studentId);
        $student->update($fields);
        return $student;
    }
}
