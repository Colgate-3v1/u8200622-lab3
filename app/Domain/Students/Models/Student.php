<?php

namespace App\Domain\Students\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    public $timestamps = false;
    
    protected $table = 'students';

    protected $fillable = [
        'full_name', 'birthday', 'dormitory', 'group_id',
    ];

    public function groups(): BelongsTo
    {
        return $this->belongsTo(Group::class);
    }
}
