<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\ApiV1\Modules\Groups\Controllers\GroupController;
use App\Http\ApiV1\Modules\Students\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//groups
Route::get("/v1/groups/{id}", [GroupController::class, 'get']);
Route::post("/v1/groups", [GroupController::class, 'post']);
Route::get("/v1/groups", [GroupController::class, 'getlist']);
Route::delete("/v1/groups/{id}", [GroupController::class, 'delete']);
Route::patch("/v1/groups/{id}", [GroupController::class, 'patch']);
Route::put("/v1/groups/{id}", [GroupController::class, 'put']);

Route::get("/v1/students/{id}", [StudentController::class, 'get']);
Route::post("/v1/students", [StudentController::class, 'post']);
Route::get("/v1/students", [StudentController::class, 'getlist']);
Route::delete("/v1/students/{id}", [StudentController::class, 'delete']);
Route::patch("/v1/students/{id}", [StudentController::class, 'patch']);
Route::put("/v1/students/{id}", [StudentController::class, 'put']);
