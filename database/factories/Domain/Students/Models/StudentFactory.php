<?php

namespace Database\Factories\Domain\Students\Models;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Domain\Students\Models\Student;
use App\Domain\Groups\Models\Group;


/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class StudentFactory extends Factory
{
    protected $model = Student::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'full_name' => $this->faker->name(),
            'birthday' => $this->faker->date('Y-m-d','now'),
            'dormitory' => $this->faker->boolean(),
            'group_id' => Group::factory(),
        ];
    }
}
