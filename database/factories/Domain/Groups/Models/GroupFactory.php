<?php

namespace Database\Factories\Domain\Groups\Models;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Domain\Groups\Models\Group;
use App\Domain\Students\Models\Student;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class GroupFactory extends Factory
{
    protected $model = Group::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'direction' => $this->faker->text(30),
            'course_number' => $this->faker->numberBetween(1,4),
            'tutor_name' => $this->faker->name(),
        ];
    }
}
