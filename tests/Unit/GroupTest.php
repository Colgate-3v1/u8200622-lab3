<?php

namespace Tests\Unit;

use App\Domain\Groups\Models\Group;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Testing\Fluent\AssertableJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\putJson;
use function Pest\Laravel\patchJson;

uses(TestCase::class, RefreshDatabase::class);

it('get api/v1/groups - OK', function () {
    Group::factory()->count(2)->create();
    $response = getJson('/api/v1/groups');
    $response->assertStatus(200)
        ->assertJson(
            fn (AssertableJson $json) =>
            $json->has('data', 2)
        );
});

it('get api/v1/groups - BAD not found', function () {
    $response = getJson('/api/v1/fakegroups');
    $response->assertStatus(404);
});

it('get api/v1/groups/{id} - OK', function () {
    $group = Group::factory()->create();
    $response = getJson('/api/v1/groups/' . $group->id);
    $response->assertStatus(200)
        ->assertJson(fn (AssertableJson $json) =>
            $json->has(
                'data',
                fn ($json) => $json->where('id', $group->id)
            ->where('direction', $group->direction)
            ->where('course_number', $group->course_number)
            ->where('tutor_name', $group->tutor_name)
            ));
});

it('get api/v1/groups/{id} - BAD not found', function () {
    $group = Group::factory()->create();
    $response = getJson('/api/v1/groups/' . $group->id+1000);
    $response->assertStatus(404);
});

it('post api/v1/groups/ - OK', function () {
    $group = Group::factory()->raw();
    $response = postJson('/api/v1/groups', $group);
    $response->assertStatus(201)
    ->assertJson(fn (AssertableJson $json) =>
        $json->has(
            'data',
            fn ($json) => $json->whereType('id', 'integer')
        ->where('direction', $group['direction'])
        ->where('course_number', $group['course_number'])
        ->where('tutor_name', $group['tutor_name'])
        ));
    $this->assertDatabaseHas('groups', $group);
});

it('post api/v1/groups/ - BAD not found', function () {
    $group = Group::factory()->raw();
    $response = postJson('/api/v1/fakegroups', $group);
    $response->assertStatus(404);
});

it('post api/v1/groups/ - BAD incorrect meaning', function () {
    $group = Group::factory()->raw();
    $group['course_number'] = 10;
    $response = postJson('/api/v1/groups', $group);
    $response->assertStatus(422);
});

it('delete api/v1/groups/{id} - OK', function () {
    $group = Group::factory()->create();
    $response = deleteJson('/api/v1/groups/'. $group->id);
    $response->assertStatus(200);
    $this->assertDatabaseMissing('groups', $group->attributesToArray());
});

it('delete api/v1/groups/{id} - BAD not found', function () {
    $group = Group::factory()->create();
    $response = deleteJson('/api/v1/fakegroups/'. $group->id);
    $response->assertStatus(404);
});

it('put api/v1/groups/{id} - OK', function () {
    $group = Group::factory()->create();
    $putGroup = Group::factory()->raw();
    $response = putJson('/api/v1/groups/'. $group->id, $putGroup);
    $response->assertStatus(200)->assertJson(fn (AssertableJson $json) =>
        $json->has(
            'data',
            fn ($json) => $json->where('id', $group->id)
            ->where('direction', $putGroup['direction'])
            ->where('course_number', $putGroup['course_number'])
            ->where('tutor_name', $putGroup['tutor_name'])
        ));
    $this->assertDatabaseHas('groups', $putGroup);
});

it('put api/v1/groups/{id} - Bad not found', function () {
    $group = Group::factory()->create();
    $putGroup = Group::factory()->raw();
    $response = putJson('/api/v1/fakegroups/'. $group->id, $putGroup);
    $response->assertStatus(404);
});

it('put api/v1/groups/{id} - Bad incorrect meaning', function () {
    $group = Group::factory()->create();
    $putGroup = Group::factory()->raw();
    $putGroup['course_number'] = null;
    $response = putJson('/api/v1/groups/'. $group->id, $putGroup);
    $response->assertStatus(422);
});

it('patch api/v1/groups/{id} - OK', function () {
    $group = Group::factory()->create();
    $patchGroup = Group::factory()->raw();
    $response = patchJson('/api/v1/groups/'. $group->id, $patchGroup);
    $response->assertStatus(200)->assertJson(fn (AssertableJson $json) =>
        $json->has(
            'data',
            fn ($json) => $json->where('id', $group->id)
            ->where('direction', $patchGroup['direction'])
            ->where('course_number', $patchGroup['course_number'])
            ->where('tutor_name', $patchGroup['tutor_name'])
        ));
    $this->assertDatabaseHas('groups', $patchGroup);
});

it('patch api/v1/groups/{id} - BAD not found', function () {
    $group = Group::factory()->create();
    $patchGroup = Group::factory()->raw();
    $response = putJson('/api/v1/fakegroups/'. $group->id, $patchGroup);
    $response->assertStatus(404);
});

it('patch api/v1/groups/{id} - Bad incorrect meaning', function () {
    $group = Group::factory()->create();
    $patchGroup = Group::factory()->raw();
    $patchGroup['course_number'] = 10;
    $response = patchJson('/api/v1/groups/'. $group->id, $patchGroup);
    $response->assertStatus(422);
});