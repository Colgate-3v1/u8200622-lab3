<?php
namespace Tests\Unit;

use App\Domain\Students\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Testing\Fluent\AssertableJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\putJson;
use function Pest\Laravel\patchJson;

uses(TestCase::class, RefreshDatabase::class);

it('get api/v1/students - OK', function () {
    Student::factory()->count(2)->create();
    $response = getJson('/api/v1/students');
    $response->assertStatus(200)
        ->assertJson(
            fn (AssertableJson $json) =>
            $json->has('data', 2)
        );
});

it('get api/v1/students - Bad not found', function () {
    $response = getJson('/api/v1/fakestudents');
    $response->assertStatus(404);
});

it('get api/v1/students/{id} - OK', function () {
    $student = Student::factory()->create();
    $response = getJson('/api/v1/students/' . $student->id);
    $response->assertStatus(200)
        ->assertJson(fn (AssertableJson $json) =>
            $json->has(
                'data',
                fn ($json) => $json->where('id', $student->id)
            ->where('full_name', $student->full_name)
            ->where('birthday', $student->birthday)
            ->where('dormitory', $student->dormitory)
            ->where('group_id', $student->group_id)
            ));
});

it('get api/v1/students/{id} - Bad not found', function () {
    $student = Student::factory()->create();
    $response = getJson('/api/v1/students/' . $student->id+1000);
    $response->assertStatus(404);
});
it('post api/v1/students - OK', function () {
    $student = Student::factory()->raw();
    $response = postJson('api/v1/students', $student);
    $response->assertStatus(201)
    ->assertJson(fn (AssertableJson $json) =>
        $json->has(
            'data',
            fn ($json) => $json->whereType('id', 'integer')
        ->where('full_name', $student['full_name'])
        ->where('birthday', $student['birthday'])
        ->where('dormitory', $student['dormitory'])
        ->where('group_id', $student['group_id'])
        ));
    $this->assertDatabaseHas('students', $student);
});

it('post api/v1/students/ - BAD not found', function () {
    $student = Student::factory()->raw();
    $response = postJson('/api/v1/fakestudents', $student);
    $response->assertStatus(404);
});

it('post api/v1/students/ - BAD incorrect meaning', function () {
    $student = Student::factory()->raw();
    $student['full_name'] = null;
    $response = postJson('/api/v1/students', $student);
    $response->assertStatus(422);
});

it('delete api/v1/students/{id} - OK', function () {
    $student = Student::factory()->create();
    $response = deleteJson('/api/v1/students/'. $student->id);
    $response->assertStatus(200);
    $this->assertDatabaseMissing('students', $student->attributesToArray());
});

it('delete api/v1/students/{id} - BAD not found', function () {
    $student = Student::factory()->create();
    $response = deleteJson('/api/v1/fakestudents/'. $student->id);
    $response->assertStatus(404);
});

it('put api/v1/students/{id} - OK', function () {
    $student = Student::factory()->create();
    $putStudent = Student::factory()->raw();
    $response = putJson('/api/v1/students/'. $student->id, $putStudent);
    $response->assertStatus(200)->assertJson(fn (AssertableJson $json) =>
        $json->has(
            'data',
            fn ($json) => $json->where('id', $student->id)
            ->where('full_name', $putStudent['full_name'])
            ->where('birthday', $putStudent['birthday'])
            ->where('dormitory', $putStudent['dormitory'])
            ->where('group_id', $putStudent['group_id'])
        ));
    $this->assertDatabaseHas('students', $putStudent);
});

it('put api/v1/students/{id} - Bad not found', function () {
    $student = Student::factory()->create();
    $putStudent = Student::factory()->raw();
    $response = putJson('/api/v1/fakestudents/'. $student->id, $putStudent);
    $response->assertStatus(404);
});

it('put api/v1/students/{id} - Bad incorrect meaning', function () {
    $student = Student::factory()->create();
    $putStudent = Student::factory()->raw();
    $putStudent['full_name'] = null;
    $response = putJson('/api/v1/students/'. $student->id, $putStudent);
    $response->assertStatus(422);
});

it('patch api/v1/students/{id} - OK', function () {
    $student = Student::factory()->create();
    $patchStudent = Student::factory()->raw();
    $response = patchJson('/api/v1/students/'. $student->id, $patchStudent);
    $response->assertStatus(200)->assertJson(fn (AssertableJson $json) =>
        $json->has(
            'data',
            fn ($json) => $json->where('id', $student->id)
            ->where('full_name', $patchStudent['full_name'])
            ->where('birthday', $patchStudent['birthday'])
            ->where('dormitory', $patchStudent['dormitory'])
            ->where('group_id', $patchStudent['group_id'])
        ));
    $this->assertDatabaseHas('students', $patchStudent);
});

it('patch api/v1/students/{id} - BAD not found', function () {
    $student = Student::factory()->create();
    $patchStudent = Student::factory()->raw();
    $response = putJson('/api/v1/fakestudents/'. $student->id, $patchStudent);
    $response->assertStatus(404);
});

it('patch api/v1/students/{id} - Bad incorrect meaning', function () {
    $student = Student::factory()->create();
    $patchStudent = Student::factory()->raw();
    $patchStudent['full_name'] = null;
    $response = patchJson('/api/v1/students/'. $student->id, $patchStudent);
    $response->assertStatus(422);
});
